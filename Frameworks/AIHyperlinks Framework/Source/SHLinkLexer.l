%{
/* SHLinkLexer.l
 * lex source to verify and identify URL types and completeness
 *
 * Written by Stephen Holt for Adium, released under the GPL
 *
 *	Options used:	noyywrap            : act as if yywrap always returns 1
 *					8bit                : always force 8-bit chars.
 *					caseless            : case insensitive lexer
 *					never-interactive   : prevents flex from including some calls to gettty() and such
 *										  -- gives a slight performace gain.
 *					prefix=...          : replace YY_whatever with prefix - avoids symbol collisions
 *					debug               : turns on debugging output (string + accepting rule)
 *										  -- only use while editing rules, and don't commit with this on
 *										     (it generates a lot of unnecessary output and kills performace.)
 *
 *	Variables used:		uint SHStringOffset : the position of the pointer, relative to the parent string
 *											  incremented by yyleng at each yylex() call.
 *	               		 int SHValidShift	: Used only in CANONICAL start state
 *											  ensures that yyleng reports whole length of the string,
 *											  without a costly call to yymore().
 */
#define YY_NO_UNPUT
unsigned int SHStringOffset;
int SHValidShift = 0;
#include "SHLinkLexer.h"
%}


ccTLD		(ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)
sTLD		(com|edu|gov|int|mil|net|org|biz|info|name|pro)
uTLD		(aero|coop|museum|mobi|cat|jobs|travel)

domains		({ccTLD}|{sTLD}|{uTLD}|arpa|local)
%{
/*The Unicode standard, version 4.1, table 3-6, says that the highest byte that will occur in a valid UTF-8 sequence is 0xF4.*/
%}
urlSpecifier	([[:alnum:]\x80-\xf4-]+\.)+{domains}(:[0-9]+)?(\/.*)?
ipURL		([0-9]{1,3}\.){3}[0-9][0-9]?[0-9]?(:[0-9]+)?(\/.*)?
singleDomain [[:alnum:]\x80-\xf4-]+
mailSpecifier	[^:\/]+\@.+\.{domains}
jabberSpec      xmpp:.*\@.+\.{domains}(\/.*)?
aolIMSpec       aim:goim\?screenname=[^\ \t\n&]+(&message=.+)?
aolChatSpec	aim:gochat\?roomname=[^\ \t\n&]+
yahooIMSpec	ymsgr:sendim\?.+
rdarSpec	rdar:\/\/(problems?\/)?[0-9]+(&[0-9]+)*


%option noyywrap 8bit caseless never-interactive prefix="SH"

%x CANONICAL
%%

<CANONICAL>{urlSpecifier}   {SHStringOffset += SHleng; SHleng += SHValidShift; BEGIN INITIAL; return SH_URL_VALID;}
<CANONICAL>{ipURL}          {SHStringOffset += SHleng; SHleng += SHValidShift; BEGIN INITIAL; return SH_URL_VALID;}
<CANONICAL>{singleDomain}(\/.*)? {SHStringOffset += SHleng; SHleng += SHValidShift; BEGIN INITIAL; return SH_URL_VALID;}
<CANONICAL>.*		    {SHStringOffset += SHleng; BEGIN INITIAL; return SH_URL_INVALID;}

file:\/\/\/.*		    {SHStringOffset += SHleng; return SH_FILE_VALID;}

https?:\/\/		|
s?ftp:\/\/		|
feed:\/\/		|
ssh:\/\/		|
telnet:\/\/		|
rts?p:\/\/		|
irc:\/\/		|
nntp:\/\/		|
cifs:\/\/		|
smb:\/\/		|
hydra:\/\/		|
itms:\/\/		|
see:\/\/		|
afp:\/\/		|
adiumxtra:\/\/		|
webcal:\/\/		|
svn(\+ssh)?:\/\/	|
notes:\/\/		|
gopher:\/\/		    {SHStringOffset += SHleng; SHValidShift = SHleng; BEGIN CANONICAL;}

sip:{mailSpecifier}         {SHStringOffset += SHleng; SHleng += SHValidShift; BEGIN INITIAL; return SH_URL_VALID;}

mailto:{mailSpecifier}      {SHStringOffset += SHleng; return SH_MAILTO_VALID;}
{mailSpecifier}		    {SHStringOffset += SHleng; return SH_MAILTO_DEGENERATE;}

{jabberSpec}		    {SHStringOffset += SHleng; return SH_URL_VALID;}

{urlSpecifier}		    {SHStringOffset += SHleng; return SH_URL_DEGENERATE;}

{aolIMSpec}		    {SHStringOffset += SHleng; return SH_URL_VALID;}
{aolChatSpec}		    {SHStringOffset += SHleng; return SH_URL_VALID;}
{yahooIMSpec}		    {SHStringOffset += SHleng; return SH_URL_VALID;}
{rdarSpec}		    {SHStringOffset += SHleng; return SH_URL_VALID;}

.			    {return SH_URL_INVALID;}
%%
