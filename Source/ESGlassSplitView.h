//
//  ESGlassSplitView.h
//  Adium
//
//  Created by Evan Schoenberg on 6/30/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ESGlassSplitView : NSSplitView {
	NSImage		*background;
	NSSize		backgroundSize;
}

@end
